function [ pDMP, TrjDMP ] = C2DMP( pDMP, Trj )
%  ---------------------------------------------------------------------
%  Note  pDMP_integrate.m 
%  Date  August 2014
%  Authors: Tadej Petric
% 
%  Remarks: Alter trajecotry!
%  
%  REFERENCE: Petric, Tadej, et al. "Online approach for altering robot
%  behaviors based on human in the loop coaching gestures." Robotics 
%  and Automation (ICRA), 2014 IEEE International Conference on. IEEE, 2014.
% ---------------------------------------------------------------------

Length = length(Trj);
% Check and reset if necesarry new parameters!
pDMP.dt = Trj(1,2) - Trj(1,1);
pDMP.Omega = 2*pi / (Trj(1,end) - Trj(1,1));
pDMP.Phi = 0;
% Set y0
pDMP.y = Trj(2:end,1)';

sTrj = size(Trj);
TrjDMP = zeros(1+(sTrj(1)-1)*3, sTrj(2));
TrjDMP(1,:) = Trj(1,:);

pDMP.Phi = 0;
for i = 1:Length;
    pDMP = pDMP_f_C( pDMP, Trj(2:end,i) );
    
    pDMP = pDMP_integrate( pDMP );
    
    TrjDMP(2:end,i) = [pDMP.y pDMP.dy*pDMP.Omega pDMP.ddy*pDMP.Omega];
    
end


end

