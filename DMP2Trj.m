function [ TrjDMP ] = DMP2Trj( pDMP )
%  ---------------------------------------------------------------------
%  Note  pDMP_integrate.m 
%  Date  August 2014
%  Authors: Tadej Petric
% 
%  Remarks: Get trajectory!
%  
%  REFERENCE: 2012 Gams, Petric and others
% ---------------------------------------------------------------------

Steps = (pDMP.Omega / 2 / pi / pDMP.dt);

pDMP.Phi = 0;
% Set y0

TrjDMP = zeros(1+pDMP.DOF*3, Steps);

t = 0;


pDMP.Phi = 0;
for i = 1:Steps;
    pDMP = pDMP_f0( pDMP );
    
    pDMP = pDMP_integrate( pDMP );
    
    TrjDMP(1:end,i) = [t pDMP.y pDMP.dy*pDMP.Omega pDMP.ddy*pDMP.Omega];
    
    t = t + pDMP.dt;
end


end

