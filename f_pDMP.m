function [ f,w ] = f_pDMP( y,Phi,t,w_init, learn, N, lambda, h, c, dof )
%  ---------------------------------------------------------------------
%  Note  pDMP_integrate.m 
%  Date  August 2014
%  Authors: Tadej Petric
% 
%  Remarks: matlab DMP integrate
%  
%  REFERENCE: 2012 Gams, Petric and others
% ---------------------------------------------------------------------

persistent p_ w_

% Parameters
%N=25;
% lambda=1 - 1e-3;
%lambda = 0.995;
%h = 2.5*N;
r = 1;
%c = 0:2*pi/N:2*pi;
%c = c(1:N);

%dof = length(y);
% dof = 2;



% Init and state inicialization - get data from peristant variable
if t == 0;
    w = w_init;
    w_ = w;
    p = ones(N,dof);
    p_ = p;
    state_ =0;
    
else
    p = p_;
    w = w_;
end


%Temp states for calulation
w1 = zeros(N,dof);
psi = zeros(N,1);
p1 = zeros(N,dof);
er = zeros(1,dof);

%Calculating kernel funtions
for i=1:N
    psi(i) = exp(h*(cos(Phi-c(i))-1));
end


%Calculating current state

f2 = 0;
for i=1:N
    f2 = f2+psi(i);
end

for j=1:dof
    
    if learn(j) == 1
        
        for i=1:N
            
            er(j) = 1*(y(j) - w(i,j)*r);
            
            p1(i,j) = (1/lambda) * ( p(i,j) - ( (p(i,j)^2*r^2) / ( (lambda/psi(i)) + p(i,j)*r^2  ) ) );
            
            w1(i,j) =  w(i,j) + psi(i)*p(i,j)*r*er(j);
        end
        
    elseif learn(j) ==2
        for i=1:N
            er(j) = y(j);
            % %             p1(i,j) = (1/lambda) * ( p(i,j) - ( (p(i,j)^2*r^2) / ( (lambda/psi(i)) + p(i,j)*r^2  ) ) );
            p1(i,j) = 0.99;
            
            w1(i,j) =  w(i,j) + psi(i)*p1(i,j)*r*er(j);
        end
    else
        w1(:,j) = w(:,j);
        
        p1 = ones(N,dof);
    end
    
end


w_ = w1;
p_ = p1;


%Calculating new state
for i=1:dof
    f1 = 0;
    f2 = 0;
    for j=1:N
        f1 = f1+psi(j)*w_(j,i)*r;
        f2 = f2+psi(j);
    end
    f(i) = f1/f2;
end

state_ = learn;
end

