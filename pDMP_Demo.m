clear all;

Cycles = 30;

dmp = pDMP_init(50, 1, 0.01);
dmp.lambda = 0.9995;


x = sin(2*pi*(0:100)*0.01); %Sine
%x = 0:0.01:(1-0.01); % Saw

w = [];
t = [];
ts = [];
for j = 1:Cycles; 
    for i = 1:100;
        tic
        dmp = pDMP_integrate( dmp );
        dmp = pDMP_f( dmp, x(i) );
        
        
        % Store some variables
        xp(100*(j-1)+i) = x(i);
        dxp(100*(j-1)+i) = dmp.target.dz;
        ddxp(100*(j-1)+i) = dmp.target.ddz;
        
        y(100*(j-1)+i) = dmp.y;
        dy(100*(j-1)+i) = dmp.dy*dmp.Omega;
        ddy(100*(j-1)+i) = dmp.ddy*dmp.Omega;
              
        w = [w dmp.w];
        tar(100*(j-1)+i) = dmp.target.t;
        
        if isempty(t)
            t = 0;
        else
        t = [t t(end)+0.01];
        end
        f (100*(j-1)+i) = dmp.f;
        ts = [ts toc];
    end
end

for j = 1:5; 
    for i = 1:100;
        tic
        dmp = pDMP_integrate( dmp );
        dmp = pDMP_f0( dmp );
        
        
        % Store some variables
        
        xp(100*(j-1+Cycles)+i) = x(i);
        dxp(100*(j-1+Cycles)+i) = dmp.target.dz;
        ddxp(100*(j-1+Cycles)+i) = dmp.target.ddz;
        
        y(100*(j-1+Cycles)+i) = dmp.y;
        dy(100*(j-1+Cycles)+i) = dmp.dy*dmp.Omega;
        ddy(100*(j-1+Cycles)+i) = dmp.ddy*dmp.Omega;
              
        w = [w dmp.w];
        tar(100*(j-1+Cycles)+i) = dmp.target.t;
        
        if isempty(t)
            t = 0;
        else
        t = [t t(end)+0.01];
        end
        f (100*(j-1+Cycles)+i) = dmp.f;
        ts = [ts toc];
    end
    %dmp.P = dmp.P + 0.1; % add something small to keep updating
end

% Plot something here
figure(1); clf
plot(t,xp,'r','linewidth',1)
hold on
plot(t,y,'k','linewidth',1)
legend('Trj','DMP')

figure(2); clf
subplot(2,1,1)
plot(t,dxp,'r','linewidth',1)
hold on
plot(t,dy,'k','linewidth',1)
subplot(2,1,2)
plot(t,ddxp,'r','linewidth',1)
ylabel('dy')
hold on
plot(t,ddy,'k','linewidth',1)
ylabel('ddy')
xlabel('time')
legend('Trj','DMP')

figure(3); clf
plot(w')

% Variables for simulink
% x_t  = [t; xp]';
% f_t  = [t; tar]';
