clear all;

Cycles = 30;

dmp = pDMP_init(50, 2, 0.01);
dmp.lambda = 0.985;

t = 0:0.01:1;
x = sin(2*pi*t); %Sine
y = t;

Trj = [t; x; y];
%Trj = [t; x];

TrjDMP_init = DMP2Trj(dmp);

[dmp, TrjDMP] = C2DMP( dmp, Trj);
% TrjDMP - allrady calculated in the Trj2DMP
% If only trajectory needs to be calculated based on data then DMP2Trj can
% be used. 
TrjDMP = DMP2Trj(dmp);

figure(1); clf
subplot(3,1,1)
plot(TrjDMP(1,:), TrjDMP(2,:),'linewidth',2)
hold on
plot(TrjDMP(1,:), TrjDMP(3,:),'r','linewidth',2)
plot(TrjDMP_init(1,:), TrjDMP_init(2,:),'--b','linewidth',3)
plot(TrjDMP_init(1,:), TrjDMP_init(3,:),'--r','linewidth',3)

subplot(3,1,2)
plot(TrjDMP(1,:), TrjDMP(4,:))
hold on
plot(TrjDMP(1,:), TrjDMP(5,:),'r')

subplot(3,1,3)
plot(TrjDMP(1,:), TrjDMP(6,:))
hold on
plot(TrjDMP(1,:), TrjDMP(7,:),'r')

