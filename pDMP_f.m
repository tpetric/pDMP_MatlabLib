function [ pDMP ] = pDMP_f( pDMP, y_in )
%  ---------------------------------------------------------------------
%  Note  pDMP_integrate.m 
%  Date  August 2014
%  Authors: Tadej Petric
% 
%  Remarks: Calculate f() for the pDMP stucture.
%  
%  REFERENCE: 2012 Gams, Petric and others
% ---------------------------------------------------------------------

tau     = 1/pDMP.Omega;
phi = pDMP.Phi;
alpha_z = pDMP.alpha;
beta_z  = pDMP.beta;

h = pDMP.psi.h;
c = pDMP.psi.c;

g = pDMP.goal;
y = pDMP.y;
z = pDMP.dy;

P = pDMP.P;
w = pDMP.w;

dt = pDMP.dt;

dy_in = (y_in - pDMP.target.z ) / dt;
ddy_in = (dy_in - pDMP.target.dz ) / dt;
pDMP.target.z = y_in;
pDMP.target.dz = dy_in;
pDMP.target.ddz = ddy_in;

lambda = pDMP.lambda;


for i = 1:pDMP.DOF
    %calculate target for fitting
    target(i) = tau*tau*ddy_in(i) - alpha_z*(beta_z*(g(i)-y_in(i))-tau*dy_in(i));
    
    sum_all = 0; sum_psi = 0;
    
    % recursie regression
    for j = 1:pDMP.N
        % Update weights
        psi(j) = exp(h* (cos(phi-c(j))-1));
        Pu = P(j,i);
        Pu = (Pu - (Pu*Pu) / (lambda/psi(j) + Pu))/lambda ;
        w(j,i) =  w(j,i) + psi(j)*Pu*(target(i)-w(j,i));
        P(j,:) = Pu;
        % Recunstruct DMP
        sum_psi = sum_psi + psi(j);
        sum_all = sum_all + w(j,i)*psi(j);
    end
    if ( sum_psi == 0 ) % Check for not to divede by zero
        f(i) = 0;
    else
        f(i) = sum_all/sum_psi;
    end
end

pDMP.target.t = target;

pDMP.w = w;
pDMP.P = P;
pDMP.f = f;

end

