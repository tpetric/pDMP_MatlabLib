function [ pDMP ] = pDMP_f0( pDMP )
%  ---------------------------------------------------------------------
%  Note  pDMP_integrate.m 
%  Date  August 2014
%  Authors: Tadej Petric
% 
%  Remarks: Calculate f() for the pDMP stucture.
%  
%  REFERENCE: 2012 Gams, Petric and others
% ---------------------------------------------------------------------

phi = pDMP.Phi;

h = pDMP.psi.h;
c = pDMP.psi.c;

w = pDMP.w;

for i = 1:pDMP.DOF
     sum_all = 0; sum_psi = 0;
    
    % recursie regression
    for j = 1:pDMP.N
        % Update weights
        psi(j) = exp(h* (cos(phi-c(j))-1));
        % Recunstruct DMP
        sum_psi = sum_psi + psi(j);
        sum_all = sum_all + w(j,i)*psi(j);
    end
    if ( sum_psi == 0 ) % Check for not to divede by zero
        f(i) = 0;
    else
        f(i) = sum_all/sum_psi;
    end
end
pDMP.f = f;

end

