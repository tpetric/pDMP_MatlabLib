function [ pDMP ] = pDMP_init( N, DOF, dt, y0, P0, w0, alpha, beta, h )
%  ---------------------------------------------------------------------
%  Note  pDMP_init.m 
%  Date  August 2014
%  Authors: Tadej Petric
% 
%  Remarks: matlab DMP init structure
%  
%  REFERENCE: 2012 Gams, Petric and others
% ---------------------------------------------------------------------

nargin;
pDMP.DOF = DOF;
pDMP.N  = N;
pDMP.dt = dt;

pDMP.Omega = 2*pi;
pDMP.Phi = 0;

pDMP.y = zeros(1,DOF);
pDMP.dy = zeros(1,DOF);
pDMP.ddy = zeros(1,DOF);

pDMP.goal = zeros(1,DOF);

pDMP.f = zeros(1,DOF);

pDMP.lambda = 0.9999995;

pDMP.target.z = 0;
pDMP.target.dz = 0;

%# first three are always needed
if nargin == 3
    pDMP.y = zeros(1,DOF);
    pDMP.alpha = 8;
    pDMP.beta = 2;
    pDMP.P = ones(N,DOF);
    pDMP.w = zeros(N,DOF);
    pDMP.psi.h = 2.5*N;
    ct = 0:2*pi/N:2*pi;
    pDMP.psi.c = ct(1:N);
end

% For aditinal y0;
if nargin == 4
    if (size(y0) == [1 DOF]);
        pDMP.y =y0;
    else
        error('wrong size of y0')
    end
    pDMP.alpha = 8;
    pDMP.beta = 2;
    pDMP.P = ones(N,DOF);
    pDMP.w = zeros(N,DOF);
    
    pDMP.psi.h = 2.5*N;
    ct = 0:2*pi/N:2*pi;
    pDMP.psi.c = ct(1:N);
    
end


% For aditinal P0 and w0;
if nargin == 6
    if (size(y0) == [1 DOF]);
        pDMP.y =y0;
    else
        error('wrong size of y0')
    end
    
    if (size(P0) == [N DOF]);
        pDMP.P0 =P0;
    else
        error('wrong size of P0')
    end
    
    if (size(w0) == [N DOF]);
        pDMP.w0 =w0;
    else
        error('wrong size of w0')
    end
    
    pDMP.alpha = 8;
    pDMP.beta = 2;
    
    pDMP.psi.h = 2.5*N;
    ct = 0:2*pi/N:2*pi;
    pDMP.psi.c = ct(1:N);
end

% For aditinal P0 and w0;
if nargin == 9
    if (size(y0) == [1 DOF]);
        pDMP.y =y0;
    else
        error('wrong size of y0')
    end
    
    if (size(P0) == [N DOF]);
        pDMP.P0 =P0;
    else
        error('wrong size of P0')
    end
    
    if (size(w0) == [N DOF]);
        pDMP.w0 =w0;
    else
        error('wrong size of w0')
    end
    
    pDMP.alpha = alpha;
    pDMP.beta = beta;
    pDMP.psi.h = h*N;
    ct = 0:2*pi/N:2*pi;
    pDMP.psi.c = ct(1:N);
end



end

