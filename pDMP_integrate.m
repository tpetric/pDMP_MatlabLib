function [ pDMP ] = pDMP_integrate( pDMP )
%  ---------------------------------------------------------------------
%  Note  pDMP_integrate.m 
%  Date  August 2014
%  Authors: Tadej Petric
% 
%  Remarks: matlab DMP integrate
%  
%  REFERENCE: 2012 Gams, Petric and others
% ---------------------------------------------------------------------

tau     = 1/pDMP.Omega;
alpha_z = pDMP.alpha;
beta_z  = pDMP.beta;

g = pDMP.goal;
y = pDMP.y;
z = pDMP.dy;

f = pDMP.f;

dt = pDMP.dt;

for j=1:pDMP.DOF
    dz(j) = (1/tau)*((alpha_z *((beta_z *(g(j)-y(j)))-z(j))) + f(j));
    dy(j) = (1/tau)*z(j);
            
    %Euler integration 
    z(j) = z(j) + (dz(j) * dt);
    y(j) = y(j) + (dy(j) * dt);

end

pDMP.y   = y;
pDMP.dy  = z;
pDMP.ddy = dz; 

% Integrate phase
pDMP.Phi = pDMP.Phi + pDMP.Omega * dt; 