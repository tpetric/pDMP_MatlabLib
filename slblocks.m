function blkStruct = slblocks
  % Specify that the product should appear in the library browser
  % and be cached in its repository
  Browser.Library = 'pDMP';
  Browser.Name    = 'Periodic DMP';
  blkStruct.Browser = Browser;